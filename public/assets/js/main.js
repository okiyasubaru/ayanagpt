(function () {
  const app = document.querySelector(".app");
  const socket = io();

  let nameid;

  // login handling
  app.querySelector(".is-login #get-username").addEventListener("click", function () {
    let username = app.querySelector(".is-login #username").value;
    // console.log(username);
    if (username.length == 0) {
      return;
    }
    nameid = username;

    // connect with index.js to catch username
    socket.emit("newuser", username);

    // switch screen
    app.querySelector(".is-login").classList.remove("is-active");
    app.querySelector(".is-chat").classList.add("is-active");
  });

  // logout handling
  app.querySelector(".is-chat #is-exit").addEventListener("click", function () {
    app.querySelector(".is-login").classList.add("is-active");
    app.querySelector(".is-chat").classList.remove("is-active");
  })

  // chat handling
  app.querySelector(".is-chat #is-send").addEventListener("click", function () {
    let message = app.querySelector(".is-chat #is-message").value;
    if (message.length == 0) {
      return;
    }
    renderMessage("human", {
      username: nameid,
      text: message
    });
    socket.emit("prompt", {
      username: nameid,
      text: message
    });
  })

  socket.on("chatbot", function (message) {
    // console.log(message);

    renderMessage("Ayana", message);
  });

  function renderMessage(type, message) {
    let messageBox = app.querySelector(".is-chat .is-messages");

    if (type == "human") {
      let el = document.createElement("div");
      el.setAttribute("class", "message user-message");
      el.innerHTML = `
        <div>
          <h6 class="name">${message.username}</h6>
          <h6 class="text">${message.text}</h6>
        </div>
      `;

      messageBox.appendChild(el);
    } else if (type == "Ayana") {
        let el = document.createElement("div");
        el.setAttribute("class", "message other-message");
        el.innerHTML = `
          <div>
            <h6 class="name">${message.username}</h6>
            <h6 class="text">${message.text}</h6>
          </div>
        `;

        messageBox.appendChild(el);
    }
  }
})();

window.setInterval(function () {
  var elem = document.getElementById('is-messages');
  elem.scrollTop = elem.scrollHeight;
}, 5000);