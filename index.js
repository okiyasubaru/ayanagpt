const { Configuration, OpenAIApi } = require('openai');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config();

const express = require('express');
const path = require('path');

const app = express();
const server = require('http').createServer(app);

const io = require('socket.io')(server);

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);

// application view
app.use(express.static(path.join(__dirname + "/public")));
app.use(bodyParser.json());
app.use(cors());

// connection handling
io.on('connection', function (socket) {
  socket.on('newuser', function (username) {
    console.log("User Login : " + username);
  });

  socket.on('prompt', function (data) {
    console.log(data);

    // use OpenAI API
    const response = openai.createCompletion({
      model: "text-davinci-003",
      prompt : data.text,
      temperature : 0.2,
      max_tokens : 4000,
      top_p : 1,
      frequency_penalty : 0,
      presence_penalty: 0,
      // stop: ["\n"],
    });

    response.then((incomingData) => {
      const message = incomingData.data.choices[0].text;
      // console.log(message);

      socket.emit("chatbot", {
        username: "Ayana",
        text: message
      })
    }).catch((err)=> {
      console.log(err);
    });
  });
});

// server
server.listen(5000, () => console.log("Hai, aku adalah AyanaGPT-1."));